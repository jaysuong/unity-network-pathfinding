﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace NetworkPathfinding
{
    [Serializable]
    public struct AgentFutureTransform : IComponentData
    {
        public float3 Position;
    }

    public class AgentFutureTransformComponent : ComponentDataProxy<AgentFutureTransform>
    {
#pragma warning disable 649
        [SerializeField] private bool CopyInitialTransform;
#pragma warning restore 649

        protected override void OnEnable ()
        {
            base.OnEnable ();

            if (CopyInitialTransform && Application.isPlaying)
            {
                var position = (float3) transform.localPosition;

                Value = new AgentFutureTransform
                {
                    Position = position
                };
            }
        }
    }
}
