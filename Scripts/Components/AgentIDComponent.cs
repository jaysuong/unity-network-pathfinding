﻿using System;
using Unity.Entities;
using UnityEngine;

namespace NetworkPathfinding
{
    [Serializable]
    public struct AgentID : IComponentData
    {
        public int Value;
    }

    public class AgentIDComponent : ComponentDataProxy<AgentID>
    {
#pragma warning disable 649
        [SerializeField] private bool generateUniqueID;
#pragma warning restore 649

        protected override void OnEnable ()
        {
            base.OnEnable ();

            if (generateUniqueID && Application.isPlaying)
            {
                Value = new AgentID
                {
                    Value = gameObject.GetInstanceID ()
                };
            }
        }

    }
}
