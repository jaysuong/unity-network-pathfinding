﻿using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Networking.Transport;

namespace NetworkPathfinding
{
    [Serializable]
    public struct NetworkReadBuffer : ISharedComponentData
    {
        public List<DataStreamReader> DataStreams;
    }

    public class NetworkReadBufferComponent : SharedComponentDataProxy<NetworkReadBuffer>
    {
        protected override void OnEnable ()
        {
            base.OnEnable ();

            Value = new NetworkReadBuffer
            {
                DataStreams = new List<DataStreamReader> ()
            };
        }
    }
}
