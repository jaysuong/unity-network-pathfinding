﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace NetworkPathfinding
{
    [Serializable]
    public struct AgentDestination : IComponentData
    {
        public float3 Value;
    }

    public class AgentDestinationComponent : ComponentDataProxy<AgentDestination>
    {
#pragma warning disable 649
        [SerializeField] private bool copyInitialPosition;
#pragma warning restore 649

        protected override void OnEnable ()
        {
            base.OnEnable ();

            if (copyInitialPosition && Application.isPlaying)
            {
                var position = (float3) transform.localPosition;

                Value = new AgentDestination
                {
                    Value = position
                };
            }
        }
    }
}
