﻿using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Networking.Transport;

namespace NetworkPathfinding
{
    [Serializable]
    public struct NetworkWriteBuffer : ISharedComponentData
    {
        public List<DataStreamWriter> DataStreams;
    }

    public class NetworkWriteBufferComponent : SharedComponentDataProxy<NetworkWriteBuffer>
    {
        protected override void OnEnable ()
        {
            base.OnEnable ();

            Value = new NetworkWriteBuffer
            {
                DataStreams = new List<DataStreamWriter> ()
            };
        }
    }
}
