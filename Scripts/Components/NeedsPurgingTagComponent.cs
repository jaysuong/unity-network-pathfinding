﻿using System;
using Unity.Entities;

namespace NetworkPathfinding
{
    /// <summary>
    /// An empty tag to mark the server to purge items
    /// </summary>
    [Serializable]
    public struct NeedsPurgingTag : IComponentData { }

    public class NeedsPurgingTagComponent : ComponentDataProxy<NeedsPurgingTag> { }
}
