﻿using System;
using Unity.Entities;
using UnityEngine;

namespace NetworkPathfinding
{
    [Serializable]
    public struct AgentSimInstance : ISharedComponentData
    {
        public GameObject Prefab;
    }

    public class AgentSimInstanceComponent : SharedComponentDataProxy<AgentSimInstance>
    {

    }
}
