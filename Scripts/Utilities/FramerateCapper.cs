﻿using UnityEngine;

namespace NetworkPathfinding.Utilities
{
    /// <summary>
    /// A simple utility script that caps the game's frame rate
    /// </summary>
    public class FramerateCapper : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private FramerateType framerate;
#pragma warning restore 649

        public enum FramerateType : int
        {
            FPS15 = 15, FPS30 = 30, FPS50 = 50, FPS60 = 60, FPS100 = 100
        }

        private void Start ()
        {
            Application.targetFrameRate = (int) framerate;
        }
    }
}
