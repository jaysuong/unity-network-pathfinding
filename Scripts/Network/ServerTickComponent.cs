﻿using System;
using Unity.Entities;

namespace NetworkPathfinding
{
    [Serializable]
    public struct ServerTick : IComponentData
    {
        public float TickRate, Time;

        public float Interval => 1f / TickRate;
    }

    public class ServerTickComponent : ComponentDataProxy<ServerTick>
    {

    }
}
