﻿using System;
using System.Net;
using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;
using UnityEngine;

using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

namespace NetworkPathfinding
{
    [Serializable]
    public struct PathfindingServer : ISharedComponentData
    {
        public UdpCNetworkDriver Driver;
        public NativeList<NetworkConnection> Connections;
        public ushort Port;

        public bool HasConnections => Driver.Listening && Connections.IsCreated && Connections.Length > 0;
    }

    [RequireComponent (typeof (NetworkReadBufferComponent))]
    public class PathfindingServerComponent : SharedComponentDataProxy<PathfindingServer>
    {
        protected override void OnEnable ()
        {
            base.OnEnable ();

            var server = Value;

            var endpoint = new IPEndPoint (IPAddress.Any, server.Port);
            server.Driver = new UdpCNetworkDriver (new INetworkParameter[0]);
            server.Connections = new NativeList<NetworkConnection> (Allocator.Persistent);

            if (server.Driver.Bind (endpoint) != 0)
            {
#if SERVER_DEBUG
                Debug.LogErrorFormat ("Cannot bind to port {0}", server.Port);
#endif
            }
            else
            {
                server.Driver.Listen ();
                Value = server;
#if SERVER_DEBUG
                Debug.Log ("Server created!");
#endif
            }
        }

        protected override void OnDisable ()
        {
            base.OnDisable ();

            var server = Value;
            if (server.Driver.IsCreated)
            {
                server.Driver.Dispose ();
            }
            if (server.Connections.IsCreated)
            {
                server.Connections.Dispose ();
            }
        }
    }
}
