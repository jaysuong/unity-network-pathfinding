﻿using System;
using System.Net;
using Unity.Entities;
using Unity.Networking.Transport;
using UnityEngine;

using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

namespace NetworkPathfinding
{
    [Serializable]
    public struct PathfindingClient : ISharedComponentData
    {
        public UdpCNetworkDriver Driver;
        public NetworkConnection Connection;

        public string Address;
        public ushort Port;

        public bool IsConnected => Driver.GetConnectionState (Connection) == NetworkConnection.State.Connected;
    }

    [RequireComponent (typeof (NetworkReadBufferComponent))]
    public class PathfindingClientComponent : SharedComponentDataProxy<PathfindingClient>
    {

        protected override void OnEnable ()
        {
            base.OnEnable ();
            if (Application.isPlaying)
            {
                var client = Value;

                var endpoint = new IPEndPoint (IPAddress.Parse (client.Address), client.Port);
                client.Driver = new UdpCNetworkDriver (new INetworkParameter[0]);
                client.Connection = client.Driver.Connect (endpoint);

                Value = client;
            }
        }

        protected override void OnDisable ()
        {
            base.OnDisable ();

            var client = Value;
            if (client.Driver.IsCreated)
            {
                if (client.Driver.GetConnectionState (client.Connection) == NetworkConnection.State.Connected)
                {
                    client.Driver.Disconnect (client.Connection);
                }
                client.Driver.Dispose ();
            }
        }
    }
}
