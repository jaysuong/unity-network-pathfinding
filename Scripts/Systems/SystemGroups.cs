﻿using Unity.Entities;
using Unity.Transforms;

namespace NetworkPathfinding.Systems
{
    /// <summary>
    /// A system group used for updating network routines and reading data
    /// </summary>
    [UpdateBefore (typeof (CopyTransformFromGameObjectSystem))]
    public class NetworkRoutineGroup
    {}

    [UpdateAfter (typeof (CopyTransformFromGameObjectSystem))]
    [UpdateBefore (typeof (CopyTransformToGameObjectSystem))]
    public class PathfindingGroup
    {}

    /// <summary>
    /// A group mainly used for sending data and cleaning buffers
    /// </summary>
    [UpdateAfter (typeof (CopyTransformToGameObjectSystem))]
    public class PostNetworkRoutineGroup
    {}
}
