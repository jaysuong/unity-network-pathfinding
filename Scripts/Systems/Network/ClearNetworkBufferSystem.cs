﻿using System.Collections.Generic;
using Unity.Entities;
using Unity.Networking.Transport;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (PostNetworkRoutineGroup))]
    public class ClearNetworkBufferSystem : ComponentSystem
    {

        private ComponentGroup readGroup;
        private ComponentGroup writeGroup;

        protected override void OnCreateManager ()
        {
            readGroup = GetComponentGroup (
                ComponentType.ReadOnly<NetworkReadBuffer> ()
            );

            writeGroup = GetComponentGroup (
                ComponentType.ReadOnly<NetworkWriteBuffer> ()
            );
        }

        protected override void OnUpdate ()
        {
            var readBuffers = readGroup.GetSharedComponentDataArray<NetworkReadBuffer> ();
            for (var i = 0; i < readBuffers.Length; ++i)
            {
                readBuffers[i].DataStreams.Clear ();
            }

            var writeBuffers = writeGroup.GetSharedComponentDataArray<NetworkWriteBuffer> ();
            for (var i = 0; i < writeBuffers.Length; ++i)
            {
                var writers = writeBuffers[i].DataStreams;
                CleanDataStreams (ref writers);
            }
        }

        private void CleanDataStreams (ref List<DataStreamWriter> writers)
        {
            for (var i = 0; i < writers.Count; ++i)
            {
                writers[i].Dispose ();
            }

            writers.Clear ();
        }
    }
}
