﻿using Unity.Entities;
using Unity.Networking.Transport;
using UnityEngine;

namespace NetworkPathfinding.Systems
{
    [UpdateBefore (typeof (ClearNetworkBufferSystem))]
    [UpdateInGroup (typeof (PostNetworkRoutineGroup))]
    public class SendServerDataSystem : ComponentSystem
    {
        private ComponentGroup serverGroup;

        protected override void OnCreateManager ()
        {
            serverGroup = GetComponentGroup (
                ComponentType.ReadOnly<NetworkWriteBuffer> (),
                ComponentType.ReadOnly<PathfindingServer> (),
                typeof (ServerTick)
            );
        }

        protected override void OnUpdate ()
        {
            var buffers = serverGroup.GetSharedComponentDataArray<NetworkWriteBuffer> ();
            var servers = serverGroup.GetSharedComponentDataArray<PathfindingServer> ();
            var ticks = serverGroup.GetComponentDataArray<ServerTick> ();
            var time = Time.time;

            for (var i = 0; i < servers.Length; ++i)
            {
                var server = servers[i];
                if (!server.HasConnections)
                {
                    continue;
                }

                var tick = ticks[i];
                if (time > tick.Time)
                {
                    tick.Time = time + tick.Interval;
                    ticks[i] = tick;
                }
                else
                {
                    continue;
                }

                var streams = buffers[i].DataStreams;
                for (var k = 0; k < streams.Count; ++k)
                {
                    var stream = streams[k];
                    BroadcastData (ref server, ref stream);
                }
            }
        }

        private void BroadcastData (ref PathfindingServer server, ref DataStreamWriter stream)
        {
            var connections = server.Connections;
            for (var i = 0; i < connections.Length; ++i)
            {
                server.Driver.Send (connections[i], stream);
            }
        }
    }
}
