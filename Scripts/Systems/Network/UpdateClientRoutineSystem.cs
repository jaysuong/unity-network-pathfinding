﻿using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;
using UnityEngine;

using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (NetworkRoutineGroup))]
    public class UpdateClientRoutineSystem : ComponentSystem
    {
        private ComponentGroup networkGroup;

        protected override void OnCreateManager ()
        {
            networkGroup = GetComponentGroup (
                ComponentType.ReadOnly<PathfindingClient> (),
                ComponentType.ReadOnly<NetworkReadBuffer> ()
            );
        }

        protected override void OnUpdate ()
        {
            var cmd = PostUpdateCommands;
            var clients = networkGroup.GetSharedComponentDataArray<PathfindingClient> ();
            var buffers = networkGroup.GetSharedComponentDataArray<NetworkReadBuffer> ();
            var entities = networkGroup.GetEntityArray ();

            for (var i = 0; i < clients.Length; ++i)
            {
                var client = clients[i];
                var readBuffer = buffers[i];

                client.Driver.ScheduleUpdate ().Complete ();

                DataStreamReader reader;
                NetworkEvent.Type type;

                while ((type = client.Connection.PopEvent (client.Driver, out reader)) != NetworkEvent.Type.Empty)
                {
                    switch (type)
                    {
                    case NetworkEvent.Type.Data:
                        var buffer = readBuffer.DataStreams;
                        buffer.Add (reader);
                        cmd.SetSharedComponent (entities[i], new NetworkReadBuffer { DataStreams = buffer });
                        break;

                    case NetworkEvent.Type.Connect:
#if UNITY_EDITOR
                        Debug.LogFormat ("<color=red>[Network]</color> Client Connection Success!");
#endif
                        break;

                    case NetworkEvent.Type.Disconnect:
                        client.Connection = default (NetworkConnection);
                        break;
                    }
                }
            }
        }
    }
}
