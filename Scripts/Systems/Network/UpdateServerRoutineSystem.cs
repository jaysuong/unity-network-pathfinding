﻿using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;

using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (NetworkRoutineGroup))]
    public class UpdateServerRoutineSystem : ComponentSystem
    {
        private ComponentGroup networkGroup;

        protected override void OnCreateManager ()
        {
            networkGroup = GetComponentGroup (
                ComponentType.ReadOnly<PathfindingServer> (),
                ComponentType.ReadOnly<NetworkReadBuffer> ()
            );
        }

        protected override void OnUpdate ()
        {
            var servers = networkGroup.GetSharedComponentDataArray<PathfindingServer> ();
            var buffers = networkGroup.GetSharedComponentDataArray<NetworkReadBuffer> ();
            var entities = networkGroup.GetEntityArray ();

            for (var i = 0; i < servers.Length; ++i)
            {
                var server = servers[i];
                var buffer = buffers[i];
                var entity = entities[i];

                server.Driver.ScheduleUpdate ().Complete ();
                PruneConnections (ref server.Connections, ref entity);
                AcceptNewConnections (ref server.Driver, ref server.Connections);
                ReadConnectionStreams (ref server, ref buffer);

                PostUpdateCommands.SetSharedComponent (entity, server);
                PostUpdateCommands.SetSharedComponent (entity, buffer);
            }
        }

        private void AcceptNewConnections (ref UdpCNetworkDriver driver, ref NativeList<NetworkConnection> connections)
        {
            NetworkConnection con;

            while ((con = driver.Accept ()) != default (NetworkConnection))
            {
                if (connections.Length < 1)
                {
                    connections.Add (con);
                }
                else
                {
                    driver.Disconnect (con);
                }
            }
        }

        private void PruneConnections (ref NativeList<NetworkConnection> connections, ref Entity serverEntity)
        {
            var isPruningNeeded = false;

            for (var i = 0; i < connections.Length; ++i)
            {
                if (!connections[i].IsCreated)
                {
                    connections.RemoveAtSwapBack (i--);
                    isPruningNeeded = true;
                }
            }

            if (isPruningNeeded)
            {
#if SERVER_DEBUG
                UnityEngine.Debug.Log ("Disconnected - Need to prune connections");
#endif
                PostUpdateCommands.AddComponent (serverEntity, default (NeedsPurgingTag));
            }
        }

        private void ReadConnectionStreams (ref PathfindingServer server, ref NetworkReadBuffer readBuffer)
        {
            var connections = server.Connections;
            var driver = server.Driver;

            for (var i = 0; i < connections.Length; ++i)
            {
                var connection = connections[i];
                DataStreamReader reader;
                NetworkEvent.Type type;

                while ((type = connection.PopEvent (driver, out reader)) != NetworkEvent.Type.Empty)
                {
                    switch (type)
                    {
                        case NetworkEvent.Type.Data:
                            var buffer = readBuffer.DataStreams;
                            buffer.Add (reader);
                            readBuffer = new NetworkReadBuffer
                            {
                                DataStreams = buffer
                            };
                            break;

                        case NetworkEvent.Type.Connect:
                            break;

                        case NetworkEvent.Type.Disconnect:
                            connections[i] = default (NetworkConnection);
#if SERVER_DEBUG
                            UnityEngine.Debug.Log ("Recieved a disconnect event. Closing connection");
#endif
                            break;
                    }
                }

                if (driver.GetConnectionState (connection) == NetworkConnection.State.Disconnected)
                {

                    connections[i] = default (NetworkConnection);
#if SERVER_DEBUG
                    UnityEngine.Debug.Log ("Disconnected");
#endif
                }
            }

            server.Connections = connections;
        }
    }
}
