﻿using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace NetworkPathfinding.Systems
{
    using Object = UnityEngine.Object;

    [UpdateAfter (typeof (UpdateServerRoutineSystem))]
    [UpdateInGroup (typeof (NetworkRoutineGroup))]
    public class PurgeOutdatedSimsSystem : ComponentSystem
    {
        private ComponentGroup serverGroup;
        private ComponentGroup simGroup;

        private List<GameObject> destroyList;

        protected override void OnCreateManager ()
        {
            serverGroup = GetComponentGroup (
                ComponentType.ReadOnly<PathfindingServer> (),
                ComponentType.ReadOnly<NeedsPurgingTag> ()
            );

            simGroup = GetComponentGroup (
                ComponentType.ReadOnly<AgentID> (),
                typeof (Transform),
                typeof (NavMeshAgent)
            );

            destroyList = new List<GameObject> (1000);
        }

        protected override void OnUpdate ()
        {
            if (serverGroup.CalculateLength () != 0)
            {
                var gameObjects = simGroup.GetGameObjectArray ();
                Debug.Log ("purging");
                for (var i = 0; i < gameObjects.Length; ++i)
                {
                    destroyList.Add (gameObjects[i]);
                }

                destroyList.ForEach (g => Object.Destroy (g));
                destroyList.Clear ();

                // Remove the tag from the server
                var entitites = serverGroup.GetEntityArray ();
                EntityManager.RemoveComponent<NeedsPurgingTag> (entitites[0]);
            }
        }
    }
}
