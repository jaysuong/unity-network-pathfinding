﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (PathfindingGroup))]
    public class ClientGenerateSimRequestsSystem : ComponentSystem
    {
        private ComponentGroup simGroup;
        private ComponentGroup clientGroup;

        private List<DataStreamWriter> writers;

        private const int MaxSimsPerBlock = 49;
        private const int MaxBlockSize = 1373;

        protected override void OnCreateManager ()
        {
            simGroup = GetComponentGroup (
                ComponentType.ReadOnly<AgentID> (),
                ComponentType.ReadOnly<Position> (),
                ComponentType.ReadOnly<AgentDestination> ()
            );

            clientGroup = GetComponentGroup (
                ComponentType.ReadOnly<PathfindingClient> (),
                typeof (ServerTick)
            );

            writers = new List<DataStreamWriter> (10);
        }

        protected override void OnUpdate ()
        {
            if (!FetchClient (ref clientGroup, out var client, Time.time)) { return; }

            var simIds = simGroup.GetComponentDataArray<AgentID> ();
            var destinations = simGroup.GetComponentDataArray<AgentDestination> ();
            var positions = simGroup.GetComponentDataArray<Position> ();

            var streamCount = Mathf.CeilToInt (simIds.Length / (float) MaxSimsPerBlock);
            var simIndex = 0;
            var remainingWriteLength = simIds.Length * 28;

            for (var i = 0; i < streamCount; ++i)
            {
                var streamLength = (i < streamCount - 1) ? MaxBlockSize + 1 : remainingWriteLength + 1;
                var writer = new DataStreamWriter (streamLength, Allocator.Temp);

                writer.Write ((byte) NetworkDataType.SimRequest);
                var currentLength = 1;

                while (currentLength + 28 <= streamLength && simIndex < simIds.Length)
                {
                    writer.Write (simIds[simIndex].Value);

                    var pos = positions[simIndex].Value;
                    var dest = destinations[simIndex].Value;

                    writer.Write (pos.x);
                    writer.Write (pos.y);
                    writer.Write (pos.z);
                    writer.Write (dest.x);
                    writer.Write (dest.y);
                    writer.Write (dest.z);

                    simIndex++;
                    currentLength += 28;
                    remainingWriteLength -= 28;
                }

                // Append to the buffer
                writers.Add (writer);
            }

            // Send the writers on the network, and dispose
            var count = writers.Count;
            for (var i = 0; i < count; ++i)
            {
                client.Driver.Send (client.Connection, writers[i]);
            }

            // Clear the streams
            writers.ForEach (w => w.Dispose ());
            writers.Clear ();
        }

        private bool FetchClient (ref ComponentGroup group, out PathfindingClient client, float time)
        {
            var clients = group.GetSharedComponentDataArray<PathfindingClient> ();
            var ticks = group.GetComponentDataArray<ServerTick> ();

            if (clients.Length > 0)
            {
                var tick = ticks[0];
                if (time > tick.Time + tick.Interval)
                {
                    tick.Time = time;
                    ticks[0] = tick;

                    client = clients[0];
                    return true;
                }
            }

            client = default (PathfindingClient);
            return false;
        }

    }
}
