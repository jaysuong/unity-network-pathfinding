﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (PathfindingGroup))]
    [UpdateAfter (typeof (ClientGenerateSimRequestsSystem))]
    public class ClientReadSimResultsSystem : ComponentSystem
    {
        private struct ResultData : IEquatable<ResultData>
        {
            public int ID;
            public Vector3 Position;

            public bool Equals (ResultData other)
            {
                return ID == other.ID;
            }
        }

        private ComponentGroup simGroup;
        private ComponentGroup clientGroup;

        protected override void OnCreateManager ()
        {
            simGroup = GetComponentGroup (
                ComponentType.ReadOnly<AgentID> (),
                typeof (AgentFutureTransform),
                ComponentType.Subtractive<NavMeshAgent> ()
            );

            clientGroup = GetComponentGroup (
                ComponentType.ReadOnly<NetworkReadBuffer> (),
                ComponentType.ReadOnly<PathfindingClient> ()
            );
        }

        protected override void OnUpdate ()
        {
            NetworkReadBuffer readBuffer;
            if (!FetchServerInstance (ref clientGroup, out readBuffer))
            {
                return;
            }

            // Read through the buffers to find request data
            NativeList<int> reqIndices;
            FindResultBufferData (ref readBuffer, out reqIndices);

            // Generate result data
            var resultList = new NativeList<ResultData> (Allocator.Temp);
            for (var i = 0; i < reqIndices.Length; ++i)
            {
                var stream = readBuffer.DataStreams[reqIndices[i]];
                var ctx = default (DataStreamReader.Context);
                stream.ReadByte (ref ctx);

                var readByteCount = 1;
                while (readByteCount < stream.Length)
                {
                    var data = new ResultData
                    {
                        ID = stream.ReadInt (ref ctx),

                        Position = new Vector3
                        {
                        x = stream.ReadFloat (ref ctx),
                        y = stream.ReadFloat (ref ctx),
                        z = stream.ReadFloat (ref ctx)
                        }
                    };

                    resultList.Add (data);
                    readByteCount += 16;
                }
            }

            // Build an id map of existing sims
            NativeHashMap<int, int> simIdMap;
            BuildSimIDMap (ref simGroup, out simIdMap);

            var futureTransforms = simGroup.GetComponentDataArray<AgentFutureTransform> ();
            var dt = Time.deltaTime;
            for (var i = 0; i < resultList.Length; ++i)
            {
                var data = resultList[i];

                if (simIdMap.TryGetValue (data.ID, out int simIdx))
                {
                    futureTransforms[simIdx] = new AgentFutureTransform
                    {
                        Position = data.Position,
                    };
                }
            }
        }

        private void BuildSimIDMap (ref ComponentGroup group, out NativeHashMap<int, int> simMap)
        {
            var ids = group.GetComponentDataArray<AgentID> ();
            simMap = new NativeHashMap<int, int> (ids.Length, Allocator.Temp);

            for (var i = 0; i < ids.Length; ++i)
            {
                simMap.TryAdd (ids[i].Value, i);
            }
        }

        private bool FetchServerInstance (ref ComponentGroup group, out NetworkReadBuffer buffer)
        {
            var buffers = group.GetSharedComponentDataArray<NetworkReadBuffer> ();

            if (buffers.Length > 0)
            {
                buffer = buffers[0];
            }
            else
            {
                buffer = default (NetworkReadBuffer);
            }

            return buffers.Length > 0;
        }

        private void FindResultBufferData (ref NetworkReadBuffer readBuffer, out NativeList<int> indices)
        {
            indices = new NativeList<int> (Allocator.Temp);

            var streams = readBuffer.DataStreams;
            for (var i = 0; i < streams.Count; ++i)
            {
                var stream = streams[i];
                var ctx = default (DataStreamReader.Context);
                var type = (NetworkDataType) stream.ReadByte (ref ctx);

                if (type == NetworkDataType.SimResult)
                {
                    indices.Add (i);
                }
            }
        }
    }
}
