﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Jobs;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (PathfindingGroup))]
    [UpdateAfter (typeof (ServerProcessSimRequestsSystem))]
    public class ServerWriteResultsToBufferSystem : ComponentSystem
    {

        private struct TransformData : IEquatable<TransformData>
        {
            public Vector3 Position;
            public Vector3 Rotation;

            public bool Equals (TransformData other)
            {
                return Position == other.Position && Rotation == other.Rotation;
            }
        }

        private ComponentGroup simGroup;
        private ComponentGroup serverGroup;

        private const int MaxSimsPerBlock = 49;
        private const int MaxBlockSize = 1393;
        private const short SimSize = 16;

        protected override void OnCreateManager ()
        {
            simGroup = GetComponentGroup (
                ComponentType.ReadOnly<AgentID> (),
                typeof (Transform),
                typeof (NavMeshAgent)
            );

            serverGroup = GetComponentGroup (
                ComponentType.ReadOnly<NetworkWriteBuffer> (),
                ComponentType.ReadOnly<PathfindingServer> ()
            );
        }

        protected override void OnUpdate ()
        {
            NetworkWriteBuffer buffer;
            if (!FetchWriteBuffer (ref serverGroup, out buffer))
            {
                return;
            }

            var simIds = simGroup.GetComponentDataArray<AgentID> ();
            var transforms = simGroup.GetTransformAccessArray ();
            GenerateTransformResults (ref transforms, out var transformData);

            var streamCount = Mathf.CeilToInt (simIds.Length / (float) MaxSimsPerBlock);
            var simIndex = 0;
            var remainingWriteLength = simIds.Length * SimSize;

            for (var i = 0; i < streamCount; ++i)
            {
                var streamLength = (i < streamCount - 1) ? MaxBlockSize + 1 : remainingWriteLength + 1;
                var writer = new DataStreamWriter (streamLength, Allocator.Persistent);

                writer.Write ((byte) NetworkDataType.SimResult);
                var currentLength = 1;

                while (currentLength + SimSize <= streamLength && simIndex < simIds.Length)
                {
                    // Write the ID
                    writer.Write (simIds[simIndex].Value);

                    var tr = transformData[simIndex];
                    var pos = tr.Position;

                    writer.Write (pos.x);
                    writer.Write (pos.y);
                    writer.Write (pos.z);

                    simIndex++;
                    currentLength += SimSize;
                    remainingWriteLength -= SimSize;
                }

                buffer.DataStreams.Add (writer);
            }

            var networkEntities = serverGroup.GetEntityArray ();
            PostUpdateCommands.SetSharedComponent (networkEntities[0], buffer);
        }

        private bool FetchWriteBuffer (ref ComponentGroup group, out NetworkWriteBuffer buffer)
        {
            var buffers = group.GetSharedComponentDataArray<NetworkWriteBuffer> ();
            buffer = buffers.Length > 0 ? buffers[0] : default (NetworkWriteBuffer);
            return buffers.Length > 0;
        }

        private void GenerateTransformResults (ref TransformAccessArray transforms, out NativeArray<TransformData> results)
        {
            results = new NativeArray<TransformData> (transforms.length, Allocator.Temp);

            for (var i = 0; i < transforms.length; ++i)
            {
                var transform = transforms[i];
                results[i] = new TransformData
                {
                    Position = transform.localPosition,
                    Rotation = transform.localEulerAngles
                };
            }
        }
    }
}
