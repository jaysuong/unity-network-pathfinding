﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Networking.Transport;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (PathfindingGroup))]
    public class ServerProcessSimRequestsSystem : ComponentSystem
    {

        private struct RequestData : IEquatable<RequestData>
        {
            public int ID;
            public Vector3 CurrentPos;
            public Vector3 Destination;

            public bool Equals (RequestData other)
            {
                return ID == other.ID;
            }
        }

        private ComponentGroup simGroup;
        private ComponentGroup serverGroup;

        protected override void OnCreateManager ()
        {
            simGroup = GetComponentGroup (
                ComponentType.ReadOnly<AgentID> (),
                typeof (NavMeshAgent),
                typeof (Transform)
            );

            serverGroup = GetComponentGroup (
                ComponentType.ReadOnly<NetworkReadBuffer> (),
                ComponentType.ReadOnly<PathfindingServer> (),
                ComponentType.ReadOnly<AgentSimInstance> ()
            );
        }

        protected override void OnUpdate ()
        {
            // Fetch server components
            NetworkReadBuffer readBuffer;
            AgentSimInstance spawnData;

            if (!FetchServerInstance (ref serverGroup, out readBuffer, out spawnData))
            {
                return;
            }

            // Read through the buffers to find request data
            NativeList<int> reqIndices;
            FindRequestBufferData (ref readBuffer, out reqIndices);

            var simRequests = new NativeList<RequestData> (Allocator.Temp);
            for (var i = 0; i < reqIndices.Length; ++i)
            {
                var stream = readBuffer.DataStreams[reqIndices[i]];
                var ctx = default (DataStreamReader.Context);
                stream.ReadByte (ref ctx); // Consume the first byte

                var readByteCount = 1;
                while (readByteCount < stream.Length)
                {
                    // Read ID, CurrentPos, EndingPos
                    var data = new RequestData
                    {
                        ID = stream.ReadInt (ref ctx),

                        CurrentPos = new Vector3
                        {
                        x = stream.ReadFloat (ref ctx),
                        y = stream.ReadFloat (ref ctx),
                        z = stream.ReadFloat (ref ctx)
                        },

                        Destination = new Vector3
                        {
                        x = stream.ReadFloat (ref ctx),
                        y = stream.ReadFloat (ref ctx),
                        z = stream.ReadFloat (ref ctx)
                        }
                    };

                    simRequests.Add (data);
                    readByteCount += 28; // Length of data for 1 agent
                }
            }

            // Build an id map of existing sims
            NativeHashMap<int, int> simIdMap;
            BuildSimIDMap (ref simGroup, out simIdMap);
            var simBuildList = new NativeList<RequestData> (Allocator.Temp);

            var simTransforms = simGroup.GetTransformAccessArray ();
            var simAgents = simGroup.GetComponentArray<NavMeshAgent> ();

            for (var i = 0; i < simRequests.Length; ++i)
            {
                var reqData = simRequests[i];

                if (simIdMap.TryGetValue (reqData.ID, out int simIdx))
                {
                    // simTransforms[simIdx].localPosition = reqData.CurrentPos;
                    simAgents[simIdx].SetDestination (reqData.Destination);
                    simAgents[simIdx].isStopped = false;
                }
                else
                {
                    simBuildList.Add (reqData);
                }
            }

            // For every sim that does not currently exist, spawn them
            var prefab = spawnData.Prefab;
            for (var i = 0; i < simBuildList.Length; ++i)
            {
                var data = simBuildList[i];
                var clone = UnityEngine.Object.Instantiate (prefab, data.CurrentPos, Quaternion.identity);
                clone.GetComponent<NavMeshAgent> ()?.SetDestination (data.Destination);

                var entity = clone.GetComponent<GameObjectEntity> ().Entity;
                EntityManager.SetComponentData (entity, new AgentID
                {
                    Value = data.ID
                });
            }
        }

        private void BuildSimIDMap (ref ComponentGroup group, out NativeHashMap<int, int> simMap)
        {
            var ids = group.GetComponentDataArray<AgentID> ();
            simMap = new NativeHashMap<int, int> (ids.Length, Allocator.Temp);

            for (var i = 0; i < ids.Length; ++i)
            {
                simMap.TryAdd (ids[i].Value, i);
            }
        }

        private bool FetchServerInstance (ref ComponentGroup group, out NetworkReadBuffer buffer, out AgentSimInstance simInstance)
        {
            var buffers = group.GetSharedComponentDataArray<NetworkReadBuffer> ();
            var sims = group.GetSharedComponentDataArray<AgentSimInstance> ();

            if (buffers.Length > 0)
            {
                buffer = buffers[0];
                simInstance = sims[0];
            }
            else
            {
                buffer = default (NetworkReadBuffer);
                simInstance = default (AgentSimInstance);
            }

            return buffers.Length > 0;
        }

        private void FindRequestBufferData (ref NetworkReadBuffer readBuffer, out NativeList<int> indices)
        {
            indices = new NativeList<int> (Allocator.Temp);

            var streams = readBuffer.DataStreams;
            for (var i = 0; i < streams.Count; ++i)
            {
                var stream = streams[i];
                var ctx = default (DataStreamReader.Context);
                var type = (NetworkDataType) stream.ReadByte (ref ctx);

                if (type == NetworkDataType.SimRequest)
                {
                    indices.Add (i);
                }
            }
        }
    }
}
