﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace NetworkPathfinding.Systems
{
    [UpdateInGroup (typeof (PathfindingGroup))]
    [UpdateAfter (typeof (ClientReadSimResultsSystem))]
    public class ClientSimulateLocalPathfindingSystem : JobComponentSystem
    {
        [BurstCompile]
        private struct UpdateTransformJob : IJobProcessComponentData<Position, Rotation, AgentFutureTransform>
        {
            public float3 Up;
            public float Dt;
            public float RotationSpeed;

            public void Execute (ref Position position, ref Rotation rotation, [ReadOnly] ref AgentFutureTransform transform)
            {
                var current = position.Value;
                position = new Position
                {
                    Value = math.lerp (current, transform.Position, Dt * 8f)
                };

                var diff = transform.Position - current;
                diff.y = 0f;
                var sqrMag = diff.x * diff.x + diff.z * diff.z;

                if (sqrMag > 0.001f)
                {
                    var finalRot = quaternion.LookRotation (diff, Up);
                    rotation = new Rotation
                    {
                        Value = math.slerp (rotation.Value, finalRot, RotationSpeed)
                    };
                }
            }
        }

        protected override JobHandle OnUpdate (JobHandle handle)
        {
            var job = new UpdateTransformJob
            {
                Up = new float3 (0f, 1f, 0f),
                RotationSpeed = 2f * Mathf.PI * Time.deltaTime,
                Dt = Time.deltaTime
            };
            return job.Schedule (this, handle);
        }
    }
}
