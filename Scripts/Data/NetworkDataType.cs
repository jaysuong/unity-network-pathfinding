﻿namespace NetworkPathfinding
{
    public enum NetworkDataType : byte
    {
        None = 0,
        SimRequest = 1,
        SimResult = 2,
        Misc = 3
    }
}
